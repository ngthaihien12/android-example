package hiennt.com.android_example.activities;

import android.app.Application;

import uk.co.chrisjenx.calligraphy.CalligraphyConfig;

public class ABCApplication extends Application {
    @Override
    public void onCreate() {
        super.onCreate();
        initialCalligraphy();
    }

    private void initialCalligraphy() {
        CalligraphyConfig.initDefault(new CalligraphyConfig.Builder()
                .setDefaultFontPath("fonts/Muli-Regular.ttf")
                .setFontAttrId(uk.co.chrisjenx.calligraphy.R.attr.fontPath)
                .build()
        );
    }
}
