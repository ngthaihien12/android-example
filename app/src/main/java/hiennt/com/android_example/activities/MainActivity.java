package hiennt.com.android_example.activities;

import androidx.appcompat.app.AppCompatActivity;
import androidx.viewpager.widget.ViewPager;
import hiennt.com.android_example.R;
import hiennt.com.android_example.fragments.BlankFragment;

import android.os.Bundle;

import com.ogaclejapan.smarttablayout.SmartTabLayout;
import com.ogaclejapan.smarttablayout.utils.v4.FragmentPagerItemAdapter;
import com.ogaclejapan.smarttablayout.utils.v4.FragmentPagerItems;

import java.util.List;

public class MainActivity extends BaseActivity {
    private FragmentPagerItemAdapter mAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        showProductPage(3);
    }

    private void showProductPage(int numOfPage){
        //create creator for managing tabs
        FragmentPagerItems.Creator creator = FragmentPagerItems.with(getApplicationContext());
        //add tab names, indicate fragment for tab and provide bundle for each fragment
        for (int i=0; i< numOfPage; i++) {
            Bundle bundle = new Bundle();
            bundle.putSerializable("Constants.CATEGORY_INFO", "categoryInfo");
            creator.add("Page "+i, BlankFragment.class, bundle);
        }
        //start tabs creating process by creator.create()
        mAdapter = new FragmentPagerItemAdapter(getSupportFragmentManager(),
                creator.create());
        //viewPager manages fragments
        ViewPager viewPager = findViewById(R.id.viewpager);
        viewPager.setAdapter(mAdapter);
        viewPager.setOffscreenPageLimit(numOfPage);
        //set viewPager for SmartTabLayout
        SmartTabLayout viewPagerTab = (SmartTabLayout) findViewById(R.id.view_pager_tab);
        viewPagerTab.setViewPager(viewPager);
    }
}
