package hiennt.com.android_example.webService;

public class ConfigApi {
        public static final String BASE_URL = "http://54.169.64.6:6789/localservice/";

    public static final String BRAND_ID = "1";//default is 1 => passio
    public static final String PAGE_LIMIT = "25";
    public static final int PAGE_LIMIT_NOTIFICATION = 20;

    public interface Api {
        String LOG_IN = "login";
        String GET_LIST_PRODUCT = "getproducts";
    }
}
