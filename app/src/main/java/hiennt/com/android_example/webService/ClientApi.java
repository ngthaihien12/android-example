package hiennt.com.android_example.webService;

import hiennt.com.android_example.repository.PassioService;

public class ClientApi extends BaseApi {
    public PassioService passioService() {
        return this.getService(PassioService.class, ConfigApi.BASE_URL);
    }
}
