package hiennt.com.android_example.webService;

public interface CallBackData<T> {
    void onSuccess(T t);

    void onFail(String message);
}
