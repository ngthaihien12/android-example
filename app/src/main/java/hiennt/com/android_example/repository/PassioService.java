package hiennt.com.android_example.repository;

import hiennt.com.android_example.webService.ConfigApi;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;

public interface PassioService {
    @POST(ConfigApi.Api.LOG_IN)
    Call<ResponseBody> login(@Body RequestBody jsonObject);

    @GET(ConfigApi.Api.GET_LIST_PRODUCT)
    Call<ResponseBody> getListProduct();
}
