package hiennt.com.android_example.utils;

public class Constants {
    public static final int SPLASH_TIME_OUT = 2000;

    public static final String CATEGORY_INFO = "CATEGORY_INFO";
    public static final String BUNDLE_DRINK_OBJECT = "DRINK_OBJECT";
    public static final String BUNDLE_ORDER_ITEM = "ORDER_ITEM";

    public static final String CURRENCY = "đ";
}
